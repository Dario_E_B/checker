#!/usr/bin/python

from time import sleep
from threading import Thread
from shutil import copyfile
import subprocess, sys, os, random, string, argparse


debug_mode = False


class Antivirus:
    def __init__(self, av):
        self.av = av
        self.test = 0

    def getReport(self, path):
        if subprocess.call(["C:\\Program Files\\Norton Security\\Engine\\"+ sorted(os.listdir("C:\\Program Files\\Norton Security\\Engine\\"))[-1] +"\\MCUI32.exe", "/export", path]) == 0:
            return True
        return False 

    def check(self, malware, path):
        if self.getReport(path) == False:
            raise Exception("Impossible Create Report")
        sys.stdout.write("Weating Report...")
        while os.path.isfile(path) != True:
            sys.stdout.write(".")
            sleep(1)
        sys.stdout.write("\n")
        if malware in open(path).read().replace("\x00", ""):
            return True
        return False
        

class Malware:
    def __init__(self, player, log):
        self.player = splitPathName(player)
        self.log = splitPathName(log)
        self.players2Exec = [player, player]
        self.logs2Exec = [log, log]

    def setEnv(self, logEa, logLa):
        try:
            i = int(self.log[1][:-4])
            i = (2 << (i.bit_length()-1)) + (i - (2 << (i.bit_length()-2)))*2
        except ValueError:
            i = 2
        self.logs2Exec[0] = self.log[0] + str(i) +".log"
        self.players2Exec[0] = "C:\\Users\\cuckoo\\AppData\\Local\\Temp\\" + str(i) + ".exe"
        with open(self.logs2Exec[0], 'w+') as f0:
            f0.write(logEa.replace(self.log[0][:-4], str(i)))
            copyfile(self.getPlayer(), self.players2Exec[0])
        i = i + 1
        self.logs2Exec[1] = self.log[0] + str(i) +".log"
        self.players2Exec[1] = "C:\\Users\\cuckoo\\AppData\\Local\\Temp\\" + str(i) + ".exe"
        with open(self.logs2Exec[1], 'w+') as f1:
            f1.write(logLa.replace(self.log[0][:-4], str(i)))
            copyfile(self.getPlayer(), self.players2Exec[1])
    
    def getLog(self):
        return ''.join(self.log)

    def getPlayer(self):
        return ''.join(self.player)

    def setLog(self, log):
        self.log = splitPathName(log)


def splitFile(f):
    nlines = sum(1 for _ in f)
    print " linee: " + str(nlines)
    f.seek(0)
    if debug_mode:
        x = ''.join([f.readline() for _ in xrange(nlines)])
        return x, x
    return ''.join([f.readline() for _ in xrange(nlines/2)]), ''.join([f.readline() for _ in xrange((nlines+1)/2)])


def splitPathName(mw):
    pm = mw.split("\\")
    return '\\'.join(pm[:-1]) + "\\", pm[-1]


def runMalware(player, log, shell):
    shell.stdin.write(player + " -i " + log + " -exec\n")
    sleep(60)


def execTest(av, malw, shell):
    print malw.getLog()
    with open(malw.getLog(), 'r+') as f:
        logEa, logLa = splitFile(f)
    print "len LogEa: " + str(len(logEa)) + " len LogLa: " + str(len(logLa))
    malw.setEnv(logEa, logLa)
    runMalware(malw.players2Exec[0], malw.logs2Exec[0], shell)
    if av.check(splitPathName(malw.players2Exec[0])[1], malw.log[0] + "Report\\Report0.txt") == True:
        print "---> " + malw.players2Exec[0] +": FOUND   <---"
        if not debug_mode: execTest(av, Malware(malw.getPlayer(), malw.logs2Exec[0]), shell)
    else:
        print "---> " + malw.players2Exec[0] +": NOT Found   <---"
    runMalware(malw.players2Exec[1], malw.logs2Exec[1], shell)
    if av.check(splitPathName(malw.players2Exec[1])[1], malw.log[0] + "Report\\Report1.txt") == True:
        print "---> " + malw.players2Exec[1] +": FOUND   <---"
        if not debug_mode: execTest(av, Malware(malw.getPlayer(), malw.logs2Exec[1]), shell)
    else: 
        print "---> " + malw.players2Exec[1] +": NOT Found   <---"

def readFromShell(shell):
    print "\n##################### CMD INFORMATION ############################\n"
    while True:
        print "- " + shell.stdout.readline()[:-1]


def getShell():
    shell = subprocess.Popen(['cmd'], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    cmdId =''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))
    shell.stdin.write("tasklist /FI \"IMAGENAME eq cmd.exe\" /FI \"USERNAME eq cuckoo\"\n")
    thread = Thread(target = readFromShell, args = (shell,))
    thread.start()
    sleep(1)
    print "\n##################################################################\n"
    print 'Press Enter to run TESTS...'
    raw_input('')
    return shell, thread


def main(av, mw, pl):
    sav = Antivirus(av)
    malw = Malware(pl, mw)
    if not os.path.exists(malw.getLog()[:-4]):
        os.makedirs(malw.getLog()[:-4])
    malw.setLog(malw.getLog()[:-4] + "\\" + malw.log[1])
    copyfile(mw, malw.getLog())
    if not os.path.exists(malw.log[0] + "Report"):
        os.makedirs(malw.log[0] + "Report")
    shell, thread = getShell()
    execTest(sav, malw, shell)
    if (thread.is_alive()):
        thread._Thread__stop()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print " -h, --help\tShow Help menu"
        exit(1)
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(description='python .\\searchLog.py -a 0 -l .\\log\\malware.log -p .\\player\\playback.exe -d')
    parser.add_argument('-a', action='store', dest='av',
                        help='Select av: [0] Norton Internet Security [1] AVG [2] BullGuard\n')
    parser.add_argument('-l', action='store', dest='log_path',
                        help='Path to log')
    parser.add_argument('-p', action='store', dest='player_path',
                        help='Path to player')
    parser.add_argument('-d', action='store_true', default=False,
                        dest='debug_mode',
                        help='Debug mode')
    results = parser.parse_args()
    debug_mode = results.debug_mode
    main(results.av, results.log_path, results.player_path)
